/**
 * Created by instancetype on 6/27/14.
 */
var ansi = require('ansi')
  , cursor = ansi(process.stdout)

cursor
  .reset()
  .write(' ')
  .bold()
  .underline()
  .bg.white()
  .fg.black()
  .write('   My Cats   ')
  .fg.reset()
  .bg.reset()
  .resetUnderline()
  .resetBold()
  .write(' \n')
  .fg.green()
  .write(' ft:\n')
  .fg.yellow()
  .write('    Jitsu\n')
  .fg.grey()
  .write('    Kalymba\n')
  .reset()
