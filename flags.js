/**
 * Created by instancetype on 6/27/14.
 */
var args = process.argv.slice(2)

args.forEach(function(arg) {
  switch (arg) {

    case '-h':
    case '--help':
      printHelp()
      break
  }
})

function printHelp() {
  console.log('    usage:')
  console.log('  $ CLTool <options> <file>')
  console.log('    example:')
  console.log('  $ CLTool --color boring.txt')

  process.exit(0)
}