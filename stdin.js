/**
 * Created by instancetype on 6/27/14.
 */
const requiredAge = 18

process.stdout.write('Please enter your age: ')

process.stdin.setEncoding('utf8')

process.stdin.on('data', function(data) {
  var age = parseInt(data, 10)
  if (isNaN(age)) {
    console.log('%s is not a valid number.', data)
  } else if (age < requiredAge) {
    console.log('You must be at least %d to enter. ' +
    'Please come back in %d years.', requiredAge, requiredAge - age)
  } else {
    joinTheClub()
  }
  process.stdin.pause()
})

process.stdin.resume()

function joinTheClub() {
  console.log('Congratulations! You are now super cool!')
}