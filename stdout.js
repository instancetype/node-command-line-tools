/**
 * Created by instancetype on 6/27/14.
 */
var http = require('http')
  , url = require('url')
  , target = url.parse(process.argv[2])

http.get(target, function(res) {
  res.pipe(process.stdout)
})